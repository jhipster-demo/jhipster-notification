/**
 * View Models used by Spring MVC REST controllers.
 */
package com.company.notificationms.web.rest.vm;
